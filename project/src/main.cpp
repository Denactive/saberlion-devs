#include <cmath>
#include <fftw3.h>
#include <iostream>
#include <string.h>
#include "interface.h"

#include "../include/graphics_functions.h"
#include "level_generation.h"
#define pi 3.141592

unsigned int level_begin (Settings & sets, sf::RenderWindow & window, unsigned int & current_score) {
    unsigned int n = 0;
    unsigned int score = 0;
    obj* obj_hit = generate_level(n, score, sets);
    std::cout << "score is " << score << "; objects count is " << n << std::endl;

    int obj_count = n; // !!!

    bool **obj_hit2;
    std::vector<int> time_click;
    int tracks_count = 4;

    std::string music_file = sets.get_music_file_path();
    // const char *music_file = "../music/aw-fade-mono.wav";//(const char *) sets.get_music_file_path().c_str();
    // Music music(music_file);
    // Music music(sets.get_music_file_path());

    // int tracks_count = 4;  // колличество дорожек
    for (int i = 0; i < obj_count; ++i) {
        time_click.push_back(obj_hit[i].time);
    }
    // {12031, 14043, 15032, 16150, 17450, 19904, 22000, 22010, 22100, 22130, 24200, 35560, 36660, 36700, 40000};  // массив времени нажатия в миллисекундах (сейчас в секундах только для тестирования)
    obj_hit2 = new bool*[time_click.size()];  // массив нажатия на дорожки в нужные моменты времени
    for (size_t i = 0; i < time_click.size(); i++) {
        obj_hit2[i] = new bool[tracks_count];
    }
    // srand(time(NULL));
    for (size_t i = 0; i < time_click.size(); i++) {
        for (size_t j = 0; j < tracks_count; j++) {
            obj_hit2[i][j] = obj_hit[i].hit[j];// (bool) (rand() % 2);
        }
    }
    std::cout << "obj_hit\n";
    for (size_t i = 0; i < time_click.size(); i++) {
        for (size_t j = 0; j < tracks_count; j++) {
            std::cout<<obj_hit[i].hit[j] << " ";
        }
        std:: cout << "|";
    }
    std::cout <<"\n";
    std::cout << "time\n";
    for (int i = 0; i<time_click.size(); ++i) {
        std::cout << time_click[i] << " ";
    }
    std::cout << "\n";

    Music music(music_file);
    int song_length = music.get_duration() * 1000; // в милли_секундах
    std:: cout << "song_length: " << song_length << "\n";
    float note_speed = 1;

    current_score = graphics(tracks_count, time_click, obj_hit2, song_length, note_speed, music_file, window);
    if (current_score > score) {
        save_score(sets, current_score);
    }

    delete []obj_hit;
    delete []obj_hit2;

    return score;
}

int main(void) {
    sf::RenderWindow window(sf::VideoMode(1720, 1000), "SaberLion Game"/*, sf::Style::Fullscreen*/);
    Settings sets(SETTINGSFILE);

    while (menu(window, sets)) {
        std::cout << "run\n";
        unsigned int current_score = 0;
        unsigned int score = level_begin(sets, window, current_score);
        level_score_display(window, score, current_score);
    }

    return 0;
}
