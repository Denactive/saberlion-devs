#include "settings.h"

Settings::Settings(std::string filename) {
    if (SETTINGS_LOG)
        std::cout << "Settings ctor\n";
    std::ifstream fin(filename);
    if (!fin)
        throw "cannot open a settings file";
    while (fin) {
        std::string name;
        std::string params;
        fin >> name >> params;
        if (SETTINGS_LOG)
            std::cout << name << ' ' << params << std::endl;
        if (name == "MusicFilePath")
            music_file_path = params;
        if (name == "LevelFilePath")
            level_file_path = params;
        if (name == "PresetMusicDirectory")
            preset_music_dir = params;
        if (name == "Difficulty")
            difficulty = params;
        if (name == "TracksCount")
            tracks_count = std::stoi(params);
        if (name == "UserDirectory")
            user_dir = params;

        std::vector<int> tmp;
        if (name == "WeightVectorEasy" || name == "WeightVectorMedium" || name == "WeightVectorHard") {
            if (params != "{") {
                std::cout << "Wrong cfg file format" << std::endl;
                throw "Wrong cfg file format";
            }
            if (SETTINGS_LOG)
                std::cout << "\t";
            while (params != "}") {
                fin >> params;
                if (params == "}")
                    continue;
                int weight = std::stoi(params);
                tmp.push_back(weight);
                if (SETTINGS_LOG)
                    std::cout << weight << ' ';
            }
            if (SETTINGS_LOG)
                std::cout << "}\n";
            if (name == "WeightVectorEasy")
                weight_vector_easy = tmp;
            if (name == "WeightVectorMedium")
                weight_vector_medium = tmp;
            if (name == "WeightVectorHard")
                weight_vector_hard = tmp;
            std::cout << std::endl;
        }
    }
    fin.close();

    int dot_ptr = music_file_path.find_last_of('.');
    int slash_ptr = music_file_path.find_last_of('/');
    for (int i = slash_ptr + 1; i < dot_ptr; i++)
        track_name += music_file_path[i];

    track_format.clear();
    for (int i = dot_ptr + 1; i < music_file_path.size(); i++)
        track_format += music_file_path[i];

    if (track_format != "wav")
        std::cout << "Warning:" << track_format << " files are not supported" << std::endl;

    if (SETTINGS_LOG)
        std::cout << track_name << ' ' << track_format << '\n' << std::endl;
}

// music.wav
std::string Settings::get_music_file_path() {
    return music_file_path;
}
std::string Settings::get_level_file_path() {
    return level_file_path;
}
std::string Settings::get_difficulty() {
    return difficulty;
}

// "music" <.wav>
std::string Settings::get_track_name() {
    return track_name;
}

// .wav
std::string Settings::get_track_format() {
    return track_format;
}

int Settings::get_tracks_count() {
    return tracks_count;
}

std::vector<int> Settings::get_weight_vector_easy() {
    return weight_vector_easy;
}
std::vector<int> Settings::get_weight_vector_medium() {
    return weight_vector_medium;
}
std::vector<int> Settings::get_weight_vector_hard() {
    return weight_vector_hard;
}

std::string Settings::get_user_dir() {
    return user_dir;
}

std::string Settings::get_preset_music_dir() {
    return preset_music_dir;
}
void Settings::save_settings(std::string filename) {
    if (SETTINGS_LOG)
        std::cout << "Settings saving\n";
    std::ofstream fout;
    fout.open(filename);

    if (!fout)
        throw "cannot open a settings file";

    if (SETTINGS_LOG) {
        std::cout << "\tValues to be saved\n";
        std::cout << "\tMusicFilePath " << music_file_path << std::endl;
        std::cout << "\tLevelFilePath " << level_file_path << std::endl;
        std::cout << "\tPresetMusicDirectory " << preset_music_dir << std::endl;
        std::cout << "\tDifficulty " << difficulty << std::endl;
        std::cout << "\tTracksCount " << tracks_count << std::endl;
        std::cout << "\tUserDirectory " << user_dir << std::endl;
        std::cout << "\tWeightVectorEasy { ";
        for (int i = 0; i < weight_vector_easy.size(); i++)
            std::cout << weight_vector_easy[i] << ' ';
        std::cout << "}\n";
        std::cout << "\tWeightVectorMedium { ";
        for (int i = 0; i < weight_vector_medium.size(); i++)
            std::cout << weight_vector_medium[i] << ' ';
        std::cout << "}\n";
        std::cout << "\tWeightVectorHard { ";
        for (int i = 0; i < weight_vector_hard.size(); i++)
            std::cout << weight_vector_hard[i] << ' ';
        std::cout << "}\n";
    }
    fout << "MusicFilePath " << music_file_path << std::endl;
    fout << "LevelFilePath " << level_file_path << std::endl;
    fout << "PresetMusicDirectory " << preset_music_dir << std::endl;
    fout << "Difficulty " << difficulty << std::endl;
    fout << "TracksCount " << tracks_count << std::endl;
    fout << "UserDirectory " << user_dir << std::endl;
    fout << "WeightVectorEasy { ";
    for (int i = 0; i < weight_vector_easy.size(); i++)
        fout << weight_vector_easy[i] << ' ';
    fout << "}\n";
    fout << "WeightVectorMedium { ";
    for (int i = 0; i < weight_vector_medium.size(); i++)
        fout << weight_vector_medium[i] << ' ';
    fout << "}\n";
    fout << "WeightVectorHard { ";
    for (int i = 0; i < weight_vector_hard.size(); i++)
        fout << weight_vector_hard[i] << ' ';
    fout << "}\n";

    fout.close();
}

void Settings::get_user_dir_with_dialog() {
    char filename[1024];
    FILE *f = popen("zenity --file-selection --directory", "r");
    if (f)
        fgets(filename, 1024, f);
    int err = pclose(f);
    if (SETTINGS_LOG)
        std::cout << "pipe closed with: " << err <<std::endl;
    user_dir = std::string(filename);
    if (SETTINGS_LOG)
        std::cout << "Zenity returned (directory): " << user_dir << "(\\n guard) ->" << std::endl;
    user_dir[user_dir.size() - 1] = '/';
    //user_dir += '\0';
    if (SETTINGS_LOG)
        std::cout << "User dir now is " << user_dir << "(\\n guard) ->" << std::endl;
}

/*void Settings::get_music_path_from_user() {
    char filename[1024];
    FILE *f = popen("zenity --file-selection", "r");
    if (f)
        fgets(filename, 1024, f);
    int err = pclose(f);
    if (SETTINGS_LOG)
        std::cout << "pipe closed with: " << err <<std::endl;
    music_file_path = std::string(filename);
    if (SETTINGS_LOG)
        std::cout << "Zenity returned: " << music_file_path << "(\\n guard) ->" << std::endl;
    music_file_path[music_file_path.size() - 1] = '\0';
    if (SETTINGS_LOG)
        std::cout << "Music file path is " << music_file_path << "(\\n guard) ->" << std::endl;
}*/

void Settings::set_difficulty(std::string val) {
    difficulty = val;
}

void Settings::set_music_file_path(std::string dir) {
    music_file_path = dir;
    if (SETTINGS_LOG)
        std::cout << "Settings\n\tMusicFilePath changed to " << get_music_file_path() << std::endl;
}
