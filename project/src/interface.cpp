#include "interface.h"

int render_interface(Settings& sets, bool& show_interface_window) {
#ifndef NEW_INTERFACE
    return 0;
#else
    if (INTERFACE_LOG)
        printf("\tRender_Int start. FLG is %d\n", show_interface_window);

    if (!show_interface_window)
        return 0;

    std::cout << "begin interface render\n";
    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;
    return 0;

    // Decide GL+GLSL versions
#ifdef __APPLE__
    // GL 3.2 + GLSL 150
    const char* glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only
#endif

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH , SCREEN_HEIGHT, "Track choice", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = gladLoadGL() == 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD2)
    bool err = gladLoadGL(glfwGetProcAddress) == 0; // glad2 recommend using the windowing library loader instead of the (optionally) bundled one.
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING2)
    bool err = false;
    glbinding::Binding::initialize();
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING3)
    bool err = false;
    glbinding::initialize([](const char* name) { return (glbinding::ProcAddress)glfwGetProcAddress(name); });
#else
    bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    bool show_another_window = true;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    if (INTERFACE_LOG)
        printf("\t should close: %d\n", glfwWindowShouldClose(window));
    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        if (INTERFACE_LOG)
            printf("\t Main Loop \n");
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        //ImGui::PushItemWidth(ImGui::GetFontSize() * -12);
        //ImGui::PushItemWidth(-ImGui::GetWindowWidth() * 0.35f);

        if (show_another_window) {
            ImGui::Begin("Music file choice",
                         &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)

            // get data
            static size_t user_list_size = 0;
            static size_t preset_list_size = 0;
            static size_t file_user_list_pos = -1;
            static size_t file_preset_list_pos = -1;
            static std::string filename = sets.get_track_name() + '.' + sets.get_track_format();

            static char** user_file_list = upload_dir_files(sets.get_user_dir(), user_list_size, filename, file_user_list_pos);
            static char** preset_file_list = upload_dir_files(sets.get_preset_music_dir(), preset_list_size, filename, file_preset_list_pos);
            static char*  user_dir = shorten_dir(sets.get_user_dir());

            // draw a head
            ImGui::PushItemWidth(100);
            HelpMarker("Choose a track here. Also you can upload 'wav' music from a folder");
            ImGui::SameLine(35);

            if (ImGui::Button("Choose directory")) {
                sets.get_user_dir_with_dialog();
                for (int i = 0; i < user_list_size; i++)
                    delete [] user_file_list[i];
                delete [] user_file_list;
                user_file_list = upload_dir_files(sets.get_user_dir(), user_list_size, filename, file_user_list_pos);

                delete [] user_dir;
                user_dir = shorten_dir(sets.get_user_dir());
            }

            ImGui::SameLine(165);
            ImGui::TextColored(ImVec4(1, 1, 0, 1), "[ %5s ]", user_dir);


            // show a tooltip
            if (ImGui::IsItemHovered()) {
                ImGui::BeginTooltip();
                ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
                ImGui::TextUnformatted("Current directory\nUnicode symbols may work incorrectly");
                ImGui::PopTextWrapPos();
                ImGui::EndTooltip();
            }
            ////////////////////////////////

            // draw lists
            ImGui::PopItemWidth();
            ImGuiStyle &style = ImGui::GetStyle();

            // 2^32 - 1
            static size_t item1_current = -1;
            static size_t item2_current = -1;
            static size_t item1_previous = -1;
            static size_t item2_previous = -1;

            static float scroll_to_pos_px = 0;

            // unreleased features
            /*if (file_user_list_pos != -1)
                scroll_to_pos_px = file_user_list_pos * 10.0f;
            else
                scroll_to_pos_px = file_preset_list_pos * 10.0f;
            bool scroll_to_pos = true;
            */

            // draw lists
            float child_w = (ImGui::GetContentRegionAvail().x - 4 * style.ItemSpacing.x) / 2;
            if (child_w < 100.0f)
            child_w = 100.0f;
            ImGui::PushItemWidth(child_w);

            // TODO clicking & level construction warning
            const char *names[] = {"Tracks in a folder", "Preset tracks"};

            ImGui::PushID("##VerticalScrolling");

            ImGui::BeginGroup();
            ImGui::TextUnformatted(names[0]);

            const ImGuiWindowFlags child1_flags = 0;
            const ImGuiID child1_id = ImGui::GetID((void *) (intptr_t) 0);
            const bool child1_is_visible = ImGui::BeginChild(child1_id, ImVec2(child_w, 300.0f), true, child1_flags);

            // Todo:: this unesseccary
            //if (scroll_to_pos) {};
            //ImGui::SetScrollFromPosY(ImGui::GetCursorStartPos().y + scroll_to_pos_px, i * 0.25f);
            if (child1_is_visible) // Avoid calling SetScrollHereY when running with culled items
            {
                for (size_t item = 0; item < user_list_size; item++) {
                    if (std::string(user_file_list[item]) == filename)
                        ImGui::TextColored(ImVec4(1, 1, 0, 1), "%s", user_file_list[item]);
                    else
                        ImGui::Text("%s", user_file_list[item]);

                    if (ImGui::IsItemClicked(ImGuiMouseButton_Left) && item != item1_previous) {
                        item1_current = item;
                        if (INTERFACE_LOG)
                            printf("1) Clicked: %zu; Previous was: %zu\n", item, item1_previous);
                        item1_previous = item;
                        item2_previous = -1;
                        sets.set_music_file_path(sets.get_user_dir() + std::string(user_file_list[item]));
                        filename = std::string(user_file_list[item]);
                        sets.set_music_file_path(sets.get_user_dir() + filename);
                        if (INTERFACE_LOG)
                            printf("1) Music file changed to: %s\n", filename.c_str());
                    }
                    if (ImGui::IsItemHovered()) {
                        ImVec2 min = ImGui::GetItemRectMin();
                        ImVec2 max = ImGui::GetItemRectMax();
                        min.y = max.y;
                        ImGui::GetWindowDrawList()->AddLine(min, max, ImColor(ImVec4(1, 1, 1, 1)), 1.0f);
                        ImGui::SameLine();
                        ImGui::TextColored(ImVec4(1, 1, 0, 1), "<-");
                    }
                }
            }
            float scroll1_y = ImGui::GetScrollY();
            float scroll1_max_y = ImGui::GetScrollMaxY();
            ImGui::EndChild();
            //ImGui::Text("%.0f/%.0f", scroll_y, scroll_max_y);
            ImGui::EndGroup();

            ImGui::SameLine();
            ImGui::BeginGroup();
            ImGui::TextUnformatted(names[1]);

            const ImGuiWindowFlags child2_flags = 0;
            const ImGuiID child2_id = ImGui::GetID((void *) (intptr_t) 1);
            const bool child2_is_visible = ImGui::BeginChild(child2_id, ImVec2(child_w, 300.0f), true, child2_flags);

            if (child2_is_visible) // Avoid calling SetScrollHereY when running with culled items
            {
                for (size_t item = 0; item < preset_list_size; item++) {
                    if (std::string(preset_file_list[item]) == filename)
                        ImGui::TextColored(ImVec4(1, 1, 0, 1), "%s", preset_file_list[item]);
                    else
                        ImGui::Text("%s", preset_file_list[item]);

                    if (ImGui::IsItemClicked(ImGuiMouseButton_Left) && item != item2_previous) {
                        item2_current = item;
                        if (INTERFACE_LOG)
                            printf("2) Clicked: %zu; Previous was: %zu\n", item, item2_previous);
                        item1_previous = -1;
                        item2_previous = item;
                        filename = std::string(preset_file_list[item]);
                        sets.set_music_file_path(sets.get_preset_music_dir() + filename);
                        if (INTERFACE_LOG)
                            printf("2) Music file changed to: %s\n", filename.c_str());
                    }
                    if (ImGui::IsItemHovered()) {
                        ImVec2 min = ImGui::GetItemRectMin();
                        ImVec2 max = ImGui::GetItemRectMax();
                        min.y = max.y;
                        ImGui::GetWindowDrawList()->AddLine(min, max, ImColor(ImVec4(1, 1, 1, 1)), 1.0f);
                        ImGui::SameLine();
                        ImGui::TextColored(ImVec4(1, 1, 0, 1), "<-");
                    }
                }
            }
            float scroll2_y = ImGui::GetScrollY();
            float scroll2_max_y = ImGui::GetScrollMaxY();
            ImGui::EndChild();
            ImGui::EndGroup();

            ImGui::PopID();

            if (ImGui::Button("Close")) {
                show_another_window = false;

                if (INTERFACE_LOG)
                    printf("\nLists deletion\n");

                for (size_t i = 0; i < user_list_size; i++)
                    delete [] user_file_list[i];
                delete [] user_file_list;

                for (size_t i = 0; i < preset_list_size; i++)
                    delete [] preset_file_list[i];
                delete []  preset_file_list;

                delete [] user_dir;

                // sets.save_settings();
                if (INTERFACE_LOG)
                    printf("\nButton close\n");
            }

            // draw a current chosen filename
            ImGui::SameLine(55);
            ImGui::Text("Chosen music:");
            ImGui::SameLine(150);
            ImGui::TextColored(ImVec4(1, 1, 0, 1), "%5s", filename.c_str());

            ImGui::End();
        }

        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();
    show_interface_window = false;

    return 0;
#endif
}
