#include "sound_reader.h"

Music::Music(std::string filename) {
    m_file = fopen(filename.c_str(), "rb");
    if (!m_file) {
        std::cout << "file " << filename << " is not opened" << std::endl;
        throw "cannot load music";
    }

    // Read WAV m_header
    wav_header header = {0};
    fread(&header, sizeof(header), 1, m_file);
    // skip wExtraFormatBytes & extra format bytes
    // fseek(f, m_header.chunkSize - 16, SEEK_CUR);

    if (SOUND_READER_LOG)
        print_wav_header(&header);
    // Reading file
    header_chunk h_chunk;
    if (SOUND_READER_LOG)
        printf("id\t" "size\n");
    // go to data chunk
    while (true)
    {
        fread(&h_chunk, sizeof(chunk), 1, m_file);
        if (SOUND_READER_LOG)
            printf("%c%c%c%c\t" "%i\n", h_chunk.ID[0], h_chunk.ID[1], h_chunk.ID[2], h_chunk.ID[3], h_chunk.size);
        if (*(unsigned int *)&h_chunk.ID == 0x61746164) {

            break;
        } else {
            // time mistake here
            if (SOUND_READER_LOG)
                std::cout << "no 'data' found" << std::endl;
            // return;
        }
        //skip chunk data bytes
        fseek(m_file, h_chunk.size, SEEK_CUR);
    }

    //Number of samples
    m_sample_size = header.bitsPerSample / 8;
    m_samples_count = h_chunk.size / (header.bitsPerSample / 2);
    if (header.numChannels != 1)
        m_stereo = 1;
    m_num_channels = header.numChannels;
    m_sample_rate = header.sampleRate;
    m_bits_per_sample = header.bitsPerSample;
    m_track_length = h_chunk.size / header.byteRate;

    if (SOUND_READER_LOG) {
        std::cout << "= ADDITIONAL MUSIC INFO =" << std::endl;
        std::cout << "sample size: " << m_sample_size << " bytes " << std::endl;
        std::cout << "sample size: " << m_bits_per_sample << " bits" << std::endl;
        if (m_sample_size > 2)
            std::cout << "WARNING: 24 & 32 bits sound is not supported" << std::endl;
        std::cout << "sample count: " << m_samples_count << std::endl;
        std::cout << "sample rate: " << m_sample_rate << " samples per second" << std::endl;
        std::cout << "track length: " << m_track_length << 's' << std::endl;
        if (m_stereo)
            std::cout << "this is a stereo sound\n\tWARNING: stereo is not supported" << std::endl;
        else
            std::cout << "this is a mono sound" << std::endl;
        std::cout << "channels: " << m_num_channels << std::endl;
        std::cout << "Music constructor done." << std::endl;
    }
}

Music::~Music() {
    fclose(m_file);
}


void print_wav_header(wav_header* m_header) {
    printf("= WAV FILE HEADER =\n");
    printf("File Type: %s\n", m_header->chunkID);
    printf("File Size (bytes): %d\n", m_header->chunkSize);
    printf("WAV Marker: %s\n", m_header->format);
    printf("Format Name: %s\n", m_header->subchunk1ID);
    printf("Format Length: %d\n", m_header->subchunk1Size);
    printf("Format Type: %hd\n", m_header->audioFormat);
    printf("Number of Channels: %hd\n", m_header->numChannels);
    printf("Sample Rate: %d\n", m_header->sampleRate);
    printf("Byte Rate (Sample Rate * Bits/Sample * Channels / 8): %d\n", m_header->byteRate);
    printf("Block Align (Bits per Sample * Channels / 8.1): %hd\n", m_header->blockAlign);
    printf("Bits per Sample: %hd\n", m_header->bitsPerSample);
}

bool Music::get_chunk(chunk& ch, size_t size) {
    if (SOUND_READER_LOG)
        std::cout << "getting chunk: " << &ch << std::endl;
    m_cur_pos  = ftell(m_file);
    if (SOUND_READER_LOG)
        std::cout << "current position in the file: " << m_cur_pos << std::endl;
    if (SOUND_READER_LOG)
        if (ch.samples)
        std::cout << "the chunk contains data (ch.samples != 0). deleting..." << std::endl;
    // watch for this
    if (ch.samples)
        delete[] ch.samples;
    ch.samples = new double [size];

    size_t read_count = 0;
    size *= 2;
    if (SOUND_READER_LOG)
        std::cout << "for getting " << size/2 << " samples it is needed to read " << size << " bytes form file" << std::endl;
    unsigned char* buf = new unsigned char [size];
    // bool eof_flg = 0;
    for (read_count = 0; read_count < size; read_count++) {
        buf[read_count] = getc(m_file);
        if (DEBUG_FILE_DATA_CHECK)
            std::cout << "sym read: " << buf[read_count] << " (" << int(buf[read_count]) << ")\n";
        if (buf[read_count] == EOF) {
            // eof_flg = 1;
            if (SOUND_READER_LOG)
                std::cout << "EOF sym was found" << std::endl;
            read_count--;
            break;
        }
    }
    if (SOUND_READER_LOG)
        std::cout << "read count is " << read_count << std::endl;
    ch.samples_count = read_count / 2;
    if (read_count < size) {
        unsigned char* tmp = new unsigned char [read_count];
        for (size_t i = 0; i < read_count; i++)
            tmp[i] = buf[i];
        delete [] buf;
        buf = tmp;
    }
    ch.samples = bit_to_amplitudes(buf, read_count);
    delete [] buf;
    return true;
}

double uchar_to_double(const unsigned char& a, const unsigned char& b) {
    double res = 0;
    res = a * 256 + b;
    return res;
}

double* bit_to_amplitudes(const unsigned char* input, size_t size) {
    if (SOUND_READER_LOG)
        std::cout << "bits-to-amplitudes transformation" << std::endl;
    double* res = new double [size / 2];
    for (size_t i = 0; i < size; i += 2)
        res[i / 2] = uchar_to_double(input[i], input[i + 1]);
    return res;
}

