#include "level_generation.h"

fftw_complex* fftw(double *input, int N) {
    fftw_complex* output = new fftw_complex [N/2+1];
    fftw_plan plan = fftw_plan_dft_r2c_1d(N, input, output, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);
    return output;
}
double* ifftw(fftw_complex *input, int N) {
    double* output = new double [N];
    fftw_plan plan = fftw_plan_dft_c2r_1d(N, input, output, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);
    return output;
}

double** filterbank(fftw_complex *input, int N) {
    int nbands = 6;
    fftw_complex** output = new fftw_complex* [nbands];
    for (int i = 0; i < nbands; ++i) {
        output[i] = new fftw_complex [N];
    }
    double bandlimits[] = {0, 200, 400, 800, 1600, 3200};
    int maxfreq = 4096;
    int lim[7];
    for (int i = 0; i < nbands ; ++i) {
        lim[i] = (bandlimits[i] / maxfreq * N / 2) + 1;
    }
    lim[6] = N/2;
    for (int i = 1; i < nbands; ++i) {
        for (int j = 0; j < N; ++j) {
            output[i][j][0] = 0;
            output[i][j][1] = 0;
        }
    }
    for (int i = 1; i < nbands; ++i) {
        memcpy (output[i][lim[i]], input[lim[i]], sizeof(fftw_complex)*(lim[i+1] - lim[i]));

    }
    double ** out = new double* [nbands];
    for (int i = 0; i < nbands; ++i) {
        out[i] = ifftw(output[i], N);
        delete output[i];
    }
    delete []output;
    return out;
}
double** hwindow (double** sig, int N, int nbands) {
    double winlength = .4;
    double maxfreq = 4096;
    double hannlen = 3200;
    //hannlen = winlength*2*maxfreq;
    double* hann = new double[N];
    for (int i = 0; i < N; ++i) {
        hann[i] = 0;
    }
    //окно Ханна

    for (int i = 0; i < hannlen; ++i) {
        hann[i] = pow(cos(i * pi / hannlen / 2), 2);
    }

    // взятие по модулю и потом БПФ
    fftw_complex** sigrect = new fftw_complex* [nbands];
    for (int i = 0; i < nbands; ++i) {
        for (int j = 0; j < N; ++j) {
            if (sig[i][j] < 0) {
                sig[i][j] = sig[i][j] * (-1);
            }
        }
        sigrect[i] = fftw(sig[i], N);
    }
    //умножаем с окном Ханна и затем переводим во временное отображение
    fftw_complex* fhann = fftw(hann, N);
    delete []hann;
    fftw_complex** filtered = new fftw_complex* [nbands];
    double** output = new double*[nbands];
    for (int i = 0; i < nbands; ++i) {
        filtered[i] = new fftw_complex[N/2 + 1];
        for (int j = 0; j < N/2 + 1; ++j) {
            filtered[i][j][0] = sigrect[i][j][0] * fhann[j][0] - sigrect[i][j][1] * fhann[j][1];
            filtered[i][j][1] = sigrect[i][j][0] * fhann[j][1] + sigrect[i][j][1] * fhann[j][0];
        }
        output[i] = ifftw(filtered[i], N);
        delete[] filtered[i];
        delete[] sigrect[i];
    }
    delete[] sigrect;
    delete []fhann;
    delete []filtered;
    return output;
}
double** diffrect (double** sig, int N) {
    int nbands = 6;
    double **output = new double *[nbands];
    for (int i = 0; i < nbands; ++i) {
        output[i] = new double[N];
        for (int j = 0; j < N; ++j) {
            output[i][j] = 0;
        }
    }
    double d = 0;
    for (int i = 0; i < nbands; ++i) {
        for (int j = 1; j < N; ++j) {
            d = sig[i][j] - sig[i][j-1];
            if (d > 0) {
                output[i][j] = d;
            }
        }
    }
    return output;
}

int timecomb(double** sig, int N) {
    int minbpm = 95;
    int maxbpm = 189;
    int nbands = 6;
    fftw_complex** dft = new fftw_complex* [nbands];
    for (int i = 0; i < nbands; ++i) {
        dft[i] = fftw(sig[i], N);
    }
    double maxenergy = 0;
    int sbpm = 0;
    for (int bpm = minbpm; bpm < maxbpm; ++bpm) {
        double *fil = new double[N];
        for (int i = 0; i < N; ++i) {
            fil[i] = 0;
        }
        int nstep =  60.0/bpm * 44100;
        int npulses = 3;
        // Set every nstep samples of the filter to one
        for (int i = 0; i < npulses; ++i) {
            fil[nstep + i*nstep] = 10;
        }
        // Get the filter in the frequency domain
        fftw_complex* dftfil = fftw(fil, N);
        // Calculate the energy after convolution
        fftw_complex x;
        double e = 0;
        for (int i = 0; i < nbands; ++i) {
            double temp = 0;
            for (int j = 0; j < N/2 + 1 ; ++j) {
                x[0] = dftfil[j][0] * dft[i][j][0] - dftfil[j][1] * dft[i][j][1];
                x[1] = dftfil[j][0] * dft[i][j][1] + dftfil[j][1] * dft[i][j][0];
                temp += pow(sqrt(x[0]*x[0] + x[1]*x[1]), 2);
            }

            e += temp;
        }
        delete []dftfil;
        delete[] fil;
        //std::cout << "e=" << e;
        // If greater than all previous energies, set current bpm to the
        // bpm of the signal
        if (e > maxenergy) {
            sbpm = bpm;
            maxenergy = e;
        }
    }
    for (int i = 0; i < nbands; ++i) {
        delete dft[i];
    }
    delete [] dft;
    return sbpm;
}

std::vector<unsigned int> generate_time_codes(Music& music, Settings& sets) {
    if (GENERATION_LOG)
        std::cout << "Time codes gen...\n\tThe Track duration: " << music.get_duration() << " s" <<'\n';

    chunk chunk1 = {0};
    // длительнось отрезка в секундах
    double section_length = SECTION_LENGTH;
    // число сэмплов в отрезке section_samples_count
    const unsigned int n = section_length * music.get_sample_rate();
    int bpm = 0;  // ударов в минуту на отрезке
    // примерное число чанков
    int nchunks = music.get_duration() / section_length;
    // число частотных спектров
    const unsigned int nbands = 6;
    int time = 0;
    std::vector<unsigned int> time_codes;

    if (GENERATION_LOG)
        std::cout << "\tnchunks: " << nchunks << std::endl;

    for (int b = 0; b < nchunks; b++) {
        // считываем сэмплы на отрезке
        // Задействованная Память почистится сама
        music.get_chunk(chunk1, n);

        fftw_complex* output = fftw(chunk1.samples, n);

        double **bands = filterbank(output, n);
        delete[] output;

        double **filtered = hwindow(bands, n, 6);
        for (int k = 0; k < nbands; ++k)
            delete[] bands[k];
        delete[] bands;

        double **diff = diffrect(filtered, n);
        for (int k = 0; k < nbands; ++k)
            delete[] filtered[k];
        delete[] filtered;

        bpm = timecomb(diff, n);
        std::cout << '\t' << b+1 << ") " << bpm << '\n';
        for (int k = 0; k < nbands; ++k)
            delete[] diff[k];
        delete[] diff;

        int beats_count = section_length / 60 * bpm;   // ударов на отрезке
        int dt = section_length * 1000 / beats_count;  // интервал тайм-кодов отрисовки (мс)

        // на каждый удар - свой объект
        for (int i = 0; i < beats_count; ++i) {
            time+= dt;
            time_codes.push_back(time);
        }
    }
    delete [] chunk1.samples;
    return time_codes;
}


// TODO: srand()
obj* generate_level(unsigned int& count, unsigned int& score, Settings& sets) {
    if (GENERATION_LOG)
        std::cout << "Level generation\n";

    std::vector<unsigned int> time_codes;
    score = 0;
    count = 0;

    // Путь к треку указвать в файле конфигурации!
    Music music(sets.get_music_file_path());

    std::ifstream fin;
    std::string filename = get_lvl_filename(sets);
    if (GENERATION_LOG)
        std::cout << "Trying open " << filename << std::endl;
    fin.open(filename);

    if (!fin) {
        // generate time codes and write to file
        if (GENERATION_LOG)
            std::cout << "File " << filename << " does not exist\nCreating a new file " << filename << std::endl;

        time_codes = generate_time_codes(music, sets);
        std::ofstream fout(filename);
        if (!fout) {
            std::cout << "Failed to create a level file" << std::endl;
            throw "Failed to create a level file";
        }

        // set a previous score to 0
        // формат: скоре число. число - это 5 цифр. Если цифры нет - пробел, т.е. 0**** или 189**
        fout << "Score 0    \n";
        for (unsigned int i = 0; i < time_codes.size(); i++)
            fout << time_codes[i] << std::endl;

        fout.close();
    }
    else {
        // read file
        if (GENERATION_LOG)
            std::cout << "File " << filename << " opened successfully" << std::endl;

        std::string score_code;
        fin >> score_code >> score;
        if (score_code != "Score") {
            std::cout << "WARNING: Invalid lvl file. No Score key" << std::endl;
        }
        while (fin) {
            unsigned int t = 0;
            fin >> t;
            time_codes.push_back(t);
        }
        fin.close();
    }

    count = time_codes.size();
    obj* out = new obj[count];
    if (GENERATION_LOG)
        std::cout << "score is " << score << "; objects count is " << count << std::endl;
    // V   random filling obj.hit[j]   V

    // base, 1, 2, 3, 4, 5, 6, 7, 8
    std::vector<int> weight_vector_easy = sets.get_weight_vector_easy();
    std::vector<int> weight_vector_medium = sets.get_weight_vector_medium();
    std::vector<int> weight_vector_hard = sets.get_weight_vector_hard();

    unsigned int previous_time = 0;
    for (unsigned int i = 0; i < count; i++) {
        out[i].time = time_codes[i];
        for (int j = 0; j < sets.get_tracks_count(); ++j) {
            out[i].hit[j] = false;
        }

        int k = rand() % 100;
        int scenery = 0;
        int track = rand() % sets.get_tracks_count();
        int dt = out[i].time - previous_time;
        int track2 = 0;
        int track3 = 0;
        int track4 = 0;
        int j = 0;  // extra

        out[i].hit[track] = true;

        if (sets.get_difficulty() == "easy") {
            for (scenery = 0; weight_vector_easy[scenery] < k; scenery++);
        } else if (sets.get_difficulty() == "medium") {
            for (scenery = 0; weight_vector_medium[scenery] < k; scenery++);
        } else /*hard*/ {
            for (scenery = 0; weight_vector_hard[scenery] < k; scenery++);
        }

        // to do not cross barrier with j
        if (i >= time_codes.size() - 4)
            scenery = 0;

        if (GENERATION_LOG)
            std::cout << "scenery: " << scenery << '\n';

        // sceneries
        switch (scenery) {
            case 0:  //base
                if (GENERATION_LOG)
                    std::cout << '\t' << out[i].time << '\t' << out[i].hit[0] << out[i].hit[1] << out[i].hit[2] << out[i].hit[3] << "\n";
                break;
            case 1:
                for (j = i + 1; j < i + 4; j++) {
                    if (time_codes[j] - out[i].time == dt * (j - i)) {
                        out[j].time = time_codes[j];
                        for (int n = 0; n < sets.get_tracks_count(); n++)
                            out[j].hit[n] = false;
                        out[j].hit[track] = true;
                    } else {
                        break;
                    }
                }
                if (GENERATION_LOG)
                    for (int ii = i; ii < j; ii++)
                        std::cout << '\t' << out[ii].time << '\t' << out[ii].hit[0] << out[ii].hit[1] << out[ii].hit[2] << out[ii].hit[3] << "\n";
                i = j - 1;
                break;
            case 2: // **
                out[i].hit[track] = true;
                do
                    track2 = rand() % sets.get_tracks_count();
                while (track2 == track);
                out[i].hit[track2] = true;
                if (GENERATION_LOG)
                    std::cout << '\t' << out[i].time << '\t' << out[i].hit[0] << out[i].hit[1] << out[i].hit[2] << out[i].hit[3] << "\n";
                break;
            case 3:
                for (j = i + 1; j < i + 3; j++) {
                    if (time_codes[j] - out[i].time == dt * (j - i)) {
                        out[j].time = time_codes[j];
                        for (int n = 0; n < sets.get_tracks_count(); n++)
                            out[j].hit[n] = false;
                        out[j].hit[track] = true;
                    } else {
                        break;
                    }
                }
                if (GENERATION_LOG)
                    for (int ii = i; ii < j; ii++)
                        std::cout << '\t' << out[ii].time << '\t' << out[ii].hit[0] << out[ii].hit[1] << out[ii].hit[2] << out[ii].hit[3] << "\n";
                i = j - 1;
                break;
            case 4:
                out[i].hit[track] = false;
                do {
                    track2 = rand() % sets.get_tracks_count();
                } while (track2 == track);
                out[i].hit[track] = true;

                if (time_codes[i+3] - out[i].time == 3 * dt) {
                    out[i+1].time = time_codes[i+1];
                    out[i+2].time = time_codes[i+2];
                    out[i+3].time = time_codes[i+3];
                    for (int k = i+1; k < i + 4; k++)
                        for (int n = 0; n < sets.get_tracks_count(); n++)
                            out[k].hit[n] = false;

                    out[i+1].hit[track2] = true;
                    out[i+2].hit[track] = true;
                    out[i+3].hit[track2] = true;
                    i += 3;
                } else {
                    break;
                }
                if (GENERATION_LOG)
                    for (int ii = i-3; ii < i+1; ii++)
                        std::cout << '\t' <<  out[ii].time << '\t' << out[ii].hit[0] << out[ii].hit[1] << out[ii].hit[2] << out[ii].hit[3] << "\n";
                break;
            case 5: // ***
                do {
                    track2 = rand() % sets.get_tracks_count();
                    track3 = rand() % sets.get_tracks_count();
                }
                while (track == track2 || track == track3 || track2 == track3);
                out[i].hit[track] = true;
                out[i].hit[track2] = true;
                out[i].hit[track3] = true;
                if (GENERATION_LOG)
                    std::cout << '\t' << out[i].time << '\t' << out[i].hit[0] << out[i].hit[1] << out[i].hit[2] << out[i].hit[3] << "\n";
                break;
            case 6:
                out[i].hit[track] = false;
                out[i].hit[0] = true;
                out[i].hit[3] = true;
                if (time_codes[i+2] - out[i].time == 2 * dt) {
                    out[i+1].time = time_codes[i+1];
                    out[i+2].time = time_codes[i+2];
                    for (int k = i+1; k < i + 3; k++)
                        for (int n = 0; n < sets.get_tracks_count(); n++)
                            out[k].hit[n] = false;

                    out[i+1].hit[1] = true;
                    out[i+1].hit[2] = true;
                    out[i+2].hit[0] = true;
                    out[i+2].hit[3] = true;
                    i += 2;
                }
                if (GENERATION_LOG)
                    for (int ii = i - 2; ii <= i; ii++)
                        std::cout << '\t' << out[ii].time << '\t' << out[ii].hit[0] << out[ii].hit[1] << out[ii].hit[2] << out[ii].hit[3] << "\n";
                break;
            case 7:
                out[i].hit[track] = false;
                track = rand() % 2;
                if (track == 1) {
                    track2 = 3;
                    track3 = 0;
                    track4 = 2;
                } else {
                    track2 = 2;
                    track3 = 1;
                    track4 = 3;
                }

                out[i].hit[track] = true;
                out[i].hit[track2] = true;
                if (time_codes[i+3] - out[i].time == 3 * dt) {
                    out[i+1].time = time_codes[i+1];
                    out[i+2].time = time_codes[i+2];
                    out[i+3].time = time_codes[i+3];
                    for (int k = i+1; k < i + 4; k++)
                        for (int n = 0; n < sets.get_tracks_count(); n++)
                            out[k].hit[n] = false;

                    out[i+1].hit[track3] = true;
                    out[i+1].hit[track4] = true;
                    out[i+2].hit[track]  = true;
                    out[i+2].hit[track2] = true;
                    out[i+3].hit[track3] = true;
                    out[i+3].hit[track4] = true;
                    i += 2;
                } else {
                    break;
                }
                if (GENERATION_LOG)
                    for (int ii = i - 2; ii <= i; ii++)
                        std::cout << '\t' << out[ii].time << '\t' << out[ii].hit[0] << out[ii].hit[1] << out[ii].hit[2] << out[ii].hit[3] << "\n";
                break;
            case 8:
                out[i].hit[track] = false;
                track = rand() % 2;
                if (track == 1) {
                    track = 2;
                    track2 = 3;
                    track3 = 0;
                    track4 = 1;
                } else {
                    track2 = 1;
                    track3 = 2;
                    track4 = 3;
                }

                out[i].hit[track] = true;
                out[i].hit[track2] = true;
                if (time_codes[i+3] - out[i].time == 3 * dt) {
                    out[i+1].time = time_codes[i+1];
                    out[i+2].time = time_codes[i+2];
                    out[i+3].time = time_codes[i+3];
                    for (int k = i+1; k < i + 4; k++)
                        for (int n = 0; n < sets.get_tracks_count(); n++)
                            out[k].hit[n] = false;

                    out[i+1].hit[track3] = true;
                    out[i+1].hit[track4] = true;
                    out[i+2].hit[track] =  true;
                    out[i+2].hit[track2] = true;
                    out[i+3].hit[track3] = true;
                    out[i+3].hit[track4] = true;
                    i += 3;
                } else {
                    break;
                }
                if (GENERATION_LOG)
                    for (int ii = i-3; ii <= i; ii++)
                        std::cout << '\t' << out[ii].time << '\t' << out[ii].hit[0] << out[ii].hit[1] << out[ii].hit[2] << out[ii].hit[3] << "\n";
                break;
        }
        previous_time = out[i].time;
    }
    return out;
}

std::string get_lvl_filename(Settings& sets) {
    return sets.get_level_file_path() + sets.get_track_name() + ".lvl";
}

int save_score(Settings& sets, unsigned int score) {
    if (SETTINGS_LOG)
        std::cout << "Saving a score: " << score << " points" << std::endl;

    std::string filename = get_lvl_filename(sets);
    std::fstream fs(filename, std::fstream::in | std::fstream::out);

    std::string score_code;
    fs >> score_code;
    if (!fs) {
        std::cout << "Save_score: Failed to open " << filename << std::endl;
        throw "Cannot open file";
    }

    if (score_code != "Score") {
        std::cout << "WARNING: Invalid lvl file. No Score key" << std::endl;
        return -1;
    }

    fs << ' ' << score;
    // сохраняем 5 чисел. Если цифры нет - пробелы (чтобы символы в файле не накладывались при записи)
    int k = 0;
    int score_copy = score;
    while (score_copy / 10 != 0) {
        k++;
        score_copy /= 10;
    }

    if (SETTINGS_LOG)
        std::cout << "num spaces is " << (4 - k) << std::endl;

    // spaces filling
    for (int i = 0; i < 4 - k; i++)
        fs << ' ';

    fs.close();
    return 0;
}