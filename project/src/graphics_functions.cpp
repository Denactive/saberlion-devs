//
// updated by Sveta 20.12.2020
//

#include <sstream>
#include "graphics_functions.h"
#include "level_generation.h"
#include "interface.h"
void increase_score(sf::Text & score, float & total_score) {
    std::ostringstream playerScore;
    playerScore << int(total_score);
    score.setString("Score: " + playerScore.str());
}

void tap_notes(std::vector<Note*> notes, sf::RenderWindow *window, int tap_time, std::vector <int> &need_time, int i_need_time, sf::Text & score, float & total_score, std::vector<int> need_cells, int &success_time, double screen) {
    int tap[4] = {-1, -1, -1, -1};
    int distance_time = abs(tap_time - need_time[i_need_time]);
    int distance_time_2 = distance_time;
    if (i_need_time > 0) {
        distance_time_2 = abs(tap_time - need_time[i_need_time - 1]);
    }
    float success_koef = 0;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
        tap[0] = 0;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
        tap[1] = 1;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3)) {
        tap[2] = 2;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4)) {
        tap[3] = 3;
    }
    for (int j = 0; j < 4; j++) {
        if (tap[j] != -1) {
            for (int i = 0; i < notes.size(); i++) {
                std::cout << "tap notes track number " << notes[i]->get_track_number() << '\n';
                if (tap[j] == notes[i]->get_track_number()) {
                    notes[i]->set_sprite(930, 930, 900, 900);
                    window->draw(notes[i]->get_sprite());
                    break;
                }
            }
            size_t i = 0;
            for (; i < need_cells.size(); i++) {
                if (need_cells[i] == j) {
                    success_koef++;
                    break;
                }
            }
            if (need_cells.size() == i) {
                success_koef-=1;
            }
        }
    }
    // изменение очков игры
    if (GLOG == 2) std:: cout << "suc_time " << success_time;
    if ((distance_time < 50 || distance_time_2 < 50) && tap_time > success_time) {
        total_score += 2 * success_koef;
        success_time = tap_time;
        if (GLOG == 2) std :: cout << "success_time!";
    } else if (distance_time < 500 || distance_time_2 < 500) {
        if (success_koef < 0) success_koef = -1;
        total_score += 1 * success_koef;
        if (GLOG == 2) std::cout << "dis < 2000 ";
    } else if (distance_time >= 1500 || distance_time_2 < 1500) {
         total_score -= 2;
        if (GLOG == 2) std::cout << "dis >= 3500 ";
    } else if (distance_time >= 500 || distance_time_2 >= 500) {
        total_score -= 1;
        if (GLOG == 2) std::cout << "dis >= 2000 ";
    }
    if (total_score < 0) total_score = 0;
    increase_score(score, total_score);
    if (GLOG == 2) std::cout << " dis " << distance_time << " dis2 " << distance_time_2 << " NEW SCORE:" << total_score << "\n";
}
void coin_animation(Note * coin, float speed, float & CurrentFrame, int & rectTop) {
    coin->set_time_click(0);
    coin->set_speed(speed);
    CurrentFrame += 0.02;

    if (CurrentFrame >= 3) {
        CurrentFrame -= 3;
        if (rectTop == 0) {
            rectTop = 2700;
        } else {
            rectTop = 0;
        }
    }
    coin->set_sprite(2486 * int(CurrentFrame), rectTop, 2486, 2700);
}
void set_labels (sf::Sprite & label, int koef) {
    label.setPosition(80, 100 * (koef + 1));
    label.setScale(2.2f, 2.2f);
}
void set_labels_settings (sf::Sprite & label, int koef) {
    label.setPosition(550, 140 + 120 * (koef + 1));
    label.setScale(2.0f, 2.0f);
}
void enter_settings(sf::RenderWindow & window, sf::VideoMode screen, Settings & sets, bool& interface, bool& one_clk_flg) {
    sf::Texture background_texture, labels_texture, game_texture;
    background_texture.loadFromFile("../images/b_settings3.jpeg");
    game_texture.loadFromFile("../images/game_labels.png");
    labels_texture.loadFromFile("../images/labels_settings.png");
    sf::Sprite background(background_texture), exit(game_texture), difficulty(game_texture), select_file(game_texture);

    background.setTextureRect(sf::IntRect(0, 0, screen.width, screen.height));

    exit.setPosition(700, 400 + 180 * 3);
    exit.setScale(1.5f, 1.5f);
    difficulty.setPosition(520, 140);
    difficulty.setScale(1.7f, 1.7f);
    select_file.setPosition(520, 100 + 180 * 3);
    select_file.setScale(1.3f, 1.3f);

    sf::Sprite easy(labels_texture), medium(labels_texture), hard(labels_texture);

    std::vector<sf::Sprite> labels;
    labels.push_back(easy);
    labels.push_back(medium);
    labels.push_back(hard);
    for (int i = 0; i < 3; i++) {
        set_labels_settings(labels[i], i);
    }
    bool is_settings_menu = true;
    int settings_num = 0;
    int current_setting = -1;
    int click_counter = 0;
    while (is_settings_menu) {
        window.clear(sf::Color(0, 0, 0));

        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        for (int i = 0; i < 3; i++) {
            labels[i].setTextureRect(sf::IntRect(0, 75 * i, 300, 60));
            if (sf::IntRect(550, 140 + 120 * (i + 1), 300, 80).contains(sf::Mouse::getPosition(window))) {
                labels[i].setTextureRect(sf::IntRect(290, 75 * i, 300, 60));
                settings_num = i + 1;
            }
        }
        exit.setTextureRect(sf::IntRect(0, 0, 400, 60));
        if (sf::IntRect(700, 400+ 180 * 3, 400, 75).contains(sf::Mouse::getPosition(window))) {
            exit.setTextureRect(sf::IntRect(0, 75, 400, 60));
            settings_num = 4;
        }
        difficulty.setTextureRect(sf::IntRect(0, 75 * 3, 400, 75));
        select_file.setTextureRect(sf::IntRect(0, 75 * 4, 520, 75));
        if (sf::IntRect(520, 100 + 180 * 3, 500, 75).contains(sf::Mouse::getPosition(window))) {
            select_file.setTextureRect(sf::IntRect(0, 75 * 5, 520, 75));
            settings_num = 5;
        }

        if (sets.get_difficulty() == "easy") {
            labels[0].setTextureRect(sf::IntRect(0, 250, 300, 60));
            current_setting = 0;
        }
        if (sets.get_difficulty() == "medium") {
            labels[1].setTextureRect(sf::IntRect(0, 250 + 75, 300, 60));
            current_setting = 1;
        }
        if (sets.get_difficulty() == "hard") {
            labels[2].setTextureRect(sf::IntRect(0, 250 + 75 * 2, 300, 60));
            current_setting = 2;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            click_counter++;

            if (click_counter < 40)
                continue;
            click_counter = 0;

            std::cout << "clicked"<<settings_num<<" counter: " << click_counter <<"\n";
            if (settings_num != 0 && settings_num != 5) {
                labels[current_setting].setTextureRect(sf::IntRect(0, 75 * (current_setting), 300, 60));
            }
            if (settings_num == 1) {
                labels[0].setTextureRect(sf::IntRect(0, 250, 300, 60));
                sets.set_difficulty("easy");
                sets.save_settings(SETTINGSFILE);
            }
            if (settings_num == 2) {
                labels[1].setTextureRect(sf::IntRect(0, 250 + 75, 300, 60));
                sets.set_difficulty("medium");
                sets.save_settings(SETTINGSFILE);
            }
            if (settings_num == 3) {
                labels[2].setTextureRect(sf::IntRect(0, 250 + 75 * 2, 300, 60));
                sets.set_difficulty("hard");
                sets.save_settings(SETTINGSFILE);
            }
            if (settings_num == 4) {
                exit.setTextureRect(sf::IntRect(0, 75 * 2, 400, 60));
                is_settings_menu = false;
            }
            if (settings_num == 5 && one_clk_flg) {
                one_clk_flg = false;
                select_file.setTextureRect(sf::IntRect(0, 75 * 5, 520, 75));
                std::cout << "Trying to Render inter\n";
                render_interface(sets, interface);
                std::cout << "Render inter is done\n";
                window.display();
            }
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            is_settings_menu = false;
            std::cout << "is menu set to false \n";
        }

        window.draw(background);
        window.draw(exit);
        window.draw(select_file);
        window.draw(difficulty);
        for (int i = 0; i < 3; i++) {
            window.draw(labels[i]);
        }
        settings_num = 0;
        window.display();
    }
    std::cout << "exit settings\n";
}

void level_score_display(sf::RenderWindow & window, unsigned int total_score, unsigned int current_score) {
    sf::VideoMode screen = sf::VideoMode::getDesktopMode();

    sf::Texture background_texture, game_texture;
    background_texture.loadFromFile("../images/game_score.jpeg");
    game_texture.loadFromFile("../images/game_labels.png");
    sf::Sprite background(background_texture), close(game_texture);

    background.setPosition(screen.width * 0.2, screen.height * 0.2);
    // sf::Vector2<unsigned  int> bg = background_texture.getSize();
    background.setScale(/*float(screen.width / bg.x), float(screen.height / bg.y*/0.6f, 0.6f);
    close.setPosition(screen.width * 0.2 + 700, screen.height * 0.2 + 500);
    close.setScale(1.9f, 1.9f);

    sf::Font font;
    font.loadFromFile("../fonts/Impact Regular.ttf");
    sf::Text score_label("", font, 60), current_score_label("", font, 60);
    score_label.setFillColor(sf::Color::White); // !!
    score_label.setStyle(sf::Text::Bold);
    score_label.setPosition(screen.width * 0.2 + 100, screen.height * 0.2 + 300);
    std::ostringstream playerScore;
    playerScore << total_score;
    score_label.setString("Total score: " + playerScore.str()); // end !!

    current_score_label.setFillColor(sf::Color::White);
    current_score_label.setStyle(sf::Text::Bold);
    current_score_label.setPosition(screen.width * 0.2 + 100, screen.height * 0.2 + 200);
    std::ostringstream playerScore2;
    playerScore2 << current_score;
    current_score_label.setString("Current score: " + playerScore2.str());

    bool is_open = true;
    int tap = 0;
    while (is_open) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        close.setTextureRect(sf::IntRect(0, 75 * 6, 150, 75));
        if (sf::IntRect(screen.width * 0.2 + 700, screen.height * 0.2 + 550, 300, 75).contains(sf::Mouse::getPosition(window))) {
            close.setTextureRect(sf::IntRect(0, 75 * 7, 150, 75));
            tap = 1;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            is_open = false;
        }
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            if (tap) {
                is_open = false;
            }
        }

        window.draw(background);
        window.draw(close);
        window.draw(score_label);
        window.draw(current_score_label);
        window.display();
    }

}

int menu (sf::RenderWindow & window, Settings & sets) {
    sf::VideoMode screen = sf::VideoMode::getDesktopMode();

    sf::Texture labels_texture, menu_background, about_texture;
    labels_texture.loadFromFile("../images/labels.png");
    menu_background.loadFromFile("../images/menu_background.jpg");
    about_texture.loadFromFile("../images/top_image.png");
    sf::Sprite start(labels_texture), settings(labels_texture), about_game(labels_texture), exit(about_game),
            menu_bg(menu_background), about(about_texture);

    std::vector<sf::Sprite> labels;
    labels.push_back(start);
    labels.push_back(settings);
    labels.push_back(about_game);
    labels.push_back(exit);

    bool is_menu = 1;
    int menuNum = 0;
    int click_counter = 0;
    menu_bg.setPosition(0, 0);
    for (int i = 0; i < 4; i++) {
        set_labels(labels[i], i);
    }
    menu_bg.setTextureRect(sf::IntRect(0, 0, screen.width, screen.height));
    about.setScale(0.47f, 0.6f);

    while (is_menu) {
        menuNum = 0;
        window.clear(sf::Color(0, 0, 0));

        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        for (int i = 0; i < 4; i++) {
            labels[i].setTextureRect(sf::IntRect(0, 63 * i, 300, 60));
            if (sf::IntRect(80, 100 * (i + 1), 300, 80).contains(sf::Mouse::getPosition(window))) {
                labels[i].setTextureRect(sf::IntRect(300, 63 * i, 300, 60));
                menuNum = i + 1;
            }
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            std::cout << "Button clicked in Menu: "<< menuNum << "counter: " << click_counter <<" \n";
            click_counter++;
            if (click_counter < 40)
                continue;
            click_counter = 0;

            if (menuNum == 1) {
                labels[0].setTextureRect(sf::IntRect(0, 265, 300, 60));
                is_menu = false; // если нажали первую кнопку, то выходим из меню
            }
            if (menuNum == 2) {
                labels[1].setTextureRect(sf::IntRect(0, 265 + 63, 300, 60));
                bool flg = true;
                bool flg2 = true;
                enter_settings(window, screen, sets, flg, flg2);
            }
            if (menuNum == 3) {
                labels[2].setTextureRect(sf::IntRect(0, 265 + 63 * 2, 300, 60));
                window.draw(about);
                window.display();
                while (!sf::Keyboard::isKeyPressed(sf::Keyboard::Escape));
            }
            if (menuNum == 4) {
                labels[3].setTextureRect(sf::IntRect(0, 265 + 63 * 3, 300, 60));
                window.close();
                return 0;
            }
        }

        window.draw(menu_bg);
        for (int i = 0; i < 4; i++) {
            window.draw(labels[i]);
        }
        window.display();
    }
    return 1;
}

std::vector<int> change_cell_color(std::vector<Note*> &notes, std::vector<Note*> &notes_cell, std::vector<int> time_click, int current_time, int time_moment_end, sf::RenderWindow &window, double width) {
    std::vector<int> need_cells;
    for (size_t i = 0; i < notes.size(); i++) {
        int number_of_cell = notes[i]->get_track_number();

        need_cells.push_back(number_of_cell);

        if (notes[0]->get_y() != (notes[i]->get_y()) && i != 0) {
            break;
        }

        if (current_time < time_click[time_moment_end] && current_time > time_click[0] - 2000) {
            notes_cell[number_of_cell]->set_sprite(930, 10, 900, 900);
            float new_scale = sin(0.005 * current_time);
            if (new_scale < 0) new_scale = - new_scale;
            if (new_scale >= 0.5) {
                // notes_cell[number_of_cell]->set_scale(0.1 * float(new_scale + 0.1));
            }
        } else {
            window.draw(notes_cell[number_of_cell]->get_sprite());
            break;
        }
        window.draw(notes_cell[number_of_cell]->get_sprite());
    }
    return need_cells;
}
