//
// updated by Sveta 20.12.2020
//

#include "graphics.h"

Note::Note(float x, float y, float width, float height, std::string file, float speed):x(x), y(y), width(width), height(height), file(file), speed(speed) {
    image.loadFromFile("../images/" + file);
    image.createMaskFromColor(sf::Color(244,244,244));
    texture.loadFromImage(image);
    sprite.setTexture(texture);
    sprite.setTextureRect(sf::IntRect(10, 10, width, height));
    sprite.setPosition(x, y);
    sprite.setScale(0.1f, 0.1f);
    track_number = 0;
}
void Note::set_time_click(int new_time_click) {
    this->time_click = new_time_click;
}
int Note::get_time_click() const {
    return this->time_click;
}
void Note::set_track_number(int new_track_number) {
    this->track_number = new_track_number;
}
int Note::get_track_number() const {
    return track_number;
}
void Note::set_speed(float speed) {
    this->speed = speed;
}
void Note::set_sprite(int left, int top, int width, int height) {
    sprite.setTextureRect(sf::IntRect(left, top, width, height));
};
sf::Sprite Note::get_sprite() const {
    return sprite;
}
void Note::set_position(float x, float y) {
    this->x = x;
    this->y = y;
    sprite.setPosition(x, y);
}
void Note::move(float x, float y) {
    set_position(this->x + x, this->y + y);
}
void Note::set_scale(float new_scale) {
    sprite.setScale(new_scale, new_scale);
}
float Note::get_y() const {
    return y;
}
void Note::set_y (float new_y) {
    this->y = new_y;
    sprite.setPosition(x, y);
}
float Note::get_x() const {
    return x;
}
float Note::get_speed() const {
    return speed;
}
