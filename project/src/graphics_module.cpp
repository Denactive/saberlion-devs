//
// updated by Sveta 20.12.2020
//
#include <sstream>
#include "graphics_functions.h"
#include <ctime>
#include <cmath>
#include "level_generation.h"

unsigned int graphics(int tracks_count, std::vector<int> time_click, bool ** obj_hit, int song_length, float speed, std::string music_file, sf::RenderWindow & window) {
    int precision = 100;
    sf::VideoMode screen = sf::VideoMode::getDesktopMode();

    sf::Texture background_texture;
    background_texture.loadFromFile("../images/background_game.jpg");
    sf::Sprite background(background_texture);
    background.setTextureRect(sf::IntRect(0, 0, screen.width, screen.height));

    int time_moment_begin = 0;  // колличество моментов времени появления
    int time_moment_end = 0;  // колличество моментов времени нажатия (в один момент можно нажать на несколько нот)
    int current_time = 0;  // текущее время
    float CurrentFrame = 0;  // текущий кадр для coin (только для тестирования)
    int frame_number = 0;  // номер кадра
    int time_to_move = 0;
    int spawn_position_y = -150;

    // подключение звука и музыки
    sf::Music music;
    music.openFromFile(music_file);

    // очки
    sf::Font font;
    font.loadFromFile("../fonts/font1.ttf");
    sf::Text score("", font, 40);
    score.setFillColor(sf::Color::Red);
    score.setStyle(sf::Text::Bold);
    score.setPosition(float(screen.width * 0.8), 290);

    // сообщение об успешном нажатии
    sf::Text success("", font, 40);
    success.setFillColor(sf::Color::Green);
    success.setStyle(sf::Text::Bold);
    success.setPosition(float(screen.width * 0.8), 330);
    success.setString("Success! Excellent game!");

    // монетка (тестовый пример анимации)
    Note coin(int(screen.width * 0.8), 50, 2486, 2700, "coin.png", speed); // тест анимации

    // массивы хранения ноток и ячеек, в которых они должны нажаться
    std::vector<Note *> notes;
    std::vector<Note *> notes_cell;
    for (size_t i = 0; i < tracks_count; i++) {
        Note *note1 = new Note(float(screen.width * 0.1 * double(i + 3)), 600, 900, 900, "note.png", speed);
        note1->set_track_number(i);
        std::cout << "track number " << note1->get_track_number() << '\n';
        note1->set_sprite(10, 930, 900, 900);  // устанавливаем оранжевую нотку (это типо ячейка)
        notes_cell.push_back(note1);
    }
    
    float total_score = 0; // счет игры
    int rectTop = 0; // для анимации монетки

    window.display();

    while (time_click[time_moment_begin] < float((600 - spawn_position_y)) * 10 / speed) {
        time_click.erase(time_click.begin());
    }

    sf::Clock t_clock;

    int time2 = t_clock.getElapsedTime().asMilliseconds();
    time_to_move = time2 / 10;

    int delete_begin_time = clock() / 1000;
    std:: cout << "delete time " << delete_begin_time << "\n";

    unsigned int this_begin_time = clock();
    std::cout << "this_begin__time " << this_begin_time / 1000 << "\n";
    music.play();
    while (window.isOpen()) {
        int time = t_clock.getElapsedTime().asMilliseconds();
        current_time += time;
        t_clock.restart();
        // unsigned int time = clock();
        // current_time = ((time - this_begin_time) / 1000);


        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            break;
        }

        window.clear(sf::Color::White);
        window.draw(background);
        window.draw(coin.get_sprite());

        // очки игры, выводятся на каждом кадре, соответсвенно и поток обнуляется на каждом кадре
        increase_score(score, total_score);
        frame_number++;

        coin_animation(&coin, speed, CurrentFrame, rectTop);

        // создание нот
        // if (time_moment_end == 0) {
        //     delete_time = current_time - delete_time;
        // } else {
        //     delete_time = current_time;
        // }
        int delete_time = 0;
        if (time_moment_begin == 0) {
            // delete_time = delete_begin_time;
        }
        if (abs(time_click[time_moment_begin] - current_time + delete_time - float(600 - spawn_position_y) * 10 / speed) < precision) {
            for (size_t j = 0; j < tracks_count; j++) {
                if (obj_hit[time_moment_begin][j]) {
                    Note *note3 = new Note(float(screen.width * 0.1 * double(j + 3)), float(spawn_position_y), 900, 900, "note.png", speed);
                    note3->set_track_number(j);
                    notes.push_back(note3);
                    notes[notes.size() - 1]->set_time_click(time_moment_begin);
                    if (GLOG == 1) {
                        std::cout << "time_moment " << notes[notes.size() - 1]->get_time_click() << "|"
                              << time_moment_begin << "\n";
                    }
                }
            }
            time_moment_begin++;
        }
        if (current_time >= song_length) {
            break;
        }

        // unsigned int time2 = clock();
        // current_time = (int(time2) - this_begin_time) / 1000;
        // двигаем нотки
        if (!notes.empty()) {
            if (current_time > time_to_move) {
                for (size_t j = 0; j < notes.size(); j++) {
                    notes[j]->move(0, notes[j]->get_speed());
                    notes[j]->set_sprite(10, 10, 900, 900);
                }
                time_to_move += 10;
            }

        } else {
            window.draw(score);
            window.display();
            continue;
        }

        // рисуем ячейки
        for (size_t i = 0; i < notes_cell.size(); i++) {
            window.draw(notes_cell[i]->get_sprite());
        }

        // меняем цвет ячеек
        std::vector<int> need_cells = change_cell_color(notes, notes_cell, time_click, current_time, time_moment_end, window, screen.width);
        // нажатие на нотки
        int success_time = 1;
        tap_notes(notes, &window, current_time, time_click, time_moment_end, score, total_score, need_cells,success_time, screen.width);


        if (!notes.empty()) {
            // рисуем нотки
            bool delete_note = false;
            for (size_t i = 0; i < notes.size(); i++) {
                window.draw(notes[i]->get_sprite());
            }

            while ((current_time - (time_click[time_moment_end])) > 0/*precision*/ &&
                time_moment_end == notes[0]->get_time_click() && !notes.empty()) {

                std::cout << "Note y " << notes[0]->get_y() << " track # " << notes[0]->get_track_number() << "\n";
                std::cout << "current time " << current_time << "time_click" << time_click[time_moment_end] << "\n\n";
                unsigned int this_time = clock();
                std::cout << "this_time " << (this_time - this_begin_time) / 1000 << "\n";


                Note * tmp = notes[0];
                notes.erase(notes.begin());
                delete tmp;

                delete_note = true;
            }

            if (delete_note) {
                time_moment_end++;

                for (size_t i = 0; i < notes_cell.size(); i++) {
                    window.draw(notes_cell[i]->get_sprite());
                    notes_cell[i]->set_sprite(10, 930, 900, 900);
                    notes_cell[i]->set_scale(0.1f);
                }
            }
        }

        window.draw(score);
        window.display();
    }

    /*for (size_t i = 0; i < notes_cell.size(); i++) {
        Note * tmp = notes_cell[0];
        notes_cell.erase(notes.begin());
        delete tmp;
    }*/

    return (unsigned int) total_score;
}
