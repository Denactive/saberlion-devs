//
// updated by Sveta 20.12.2020
//

#ifndef SABERLION_DEVS_GRAPHICS_FUNCTIONS_H
#define SABERLION_DEVS_GRAPHICS_FUNCTIONS_H
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include "graphics.h"
#include "level_generation.h"
#include "interface.h"

unsigned int graphics(int tracks_count, std::vector<int> time_click, bool ** obj_hit, int song_length, float speed,
              std::string music_file, sf::RenderWindow &window);
void tap_notes(std::vector<Note*> notes, sf::RenderWindow *window, int tap_time, std::vector <int> &need_time, int i_need_time,
               sf::Text & score, float & total_score, std::vector<int> need_cells, int &success_time, double screen);
void coin_animation(Note * coin, float speed, float & CurrentFrame, int & rectTop);
void set_labels (sf::Sprite & label, int koef);
void set_labels_settings (sf::Sprite & label, int koef);
int menu (sf::RenderWindow & window, Settings & settings);
std::vector<int> change_cell_color(std::vector<Note*> &notes, std::vector<Note*> &notes_cell, std::vector<int> time_click,
                                   int current_time, int time_moment_end, sf::RenderWindow &window, double width);
void increase_score(sf::Text & score, float & total_score);
void enter_settings(sf::RenderWindow & window, sf::VideoMode screen, Settings & sets);
void level_score_display(sf::RenderWindow & window, unsigned int total_score, unsigned int current_score);
unsigned int level_begin (Settings & sets, sf::RenderWindow & window, unsigned int & current_score);

#endif // SABERLION_DEVS_GRAPHICS_FUNCTIONS_H