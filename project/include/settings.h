#ifndef PROJECT_INCLUDE_SETTINGS_H
#define PROJECT_INCLUDE_SETTINGS_H
#include <iostream>
#include <string>
#include <fstream>
#include <cstdio>
#include <vector>

#define SETTINGS_LOG 1

#define SETTINGSFILE "../settings/SaberDevs.cfg"
#define LEVELFILEPATH "../levels/"

class Settings {
public:
    //Settings();//: Settings(SETTINGSFILE) {};
    Settings(std::string filename = SETTINGSFILE);
    void save_settings(std::string filename = SETTINGSFILE);
    std::string get_music_file_path();
    std::string get_level_file_path();
    std::string get_difficulty();
    std::string get_track_name();
    std::string get_track_format();
    std::string get_user_dir();
    std::string get_preset_music_dir();
    int get_tracks_count();
    void set_difficulty(std::string val);
    void set_music_file_path(std::string dir);
    // void get_music_path_from_user();
    void get_user_dir_with_dialog();
    std::vector<int> get_weight_vector_easy();
    std::vector<int> get_weight_vector_medium();
    std::vector<int> get_weight_vector_hard();

private:
    std::string music_file_path;
    std::string level_file_path;
    std::string difficulty;
    std::string track_name;
    std::string track_format = "wav";
    std::string user_dir;
    int tracks_count = 4;
    std::vector<int> weight_vector_easy;
    std::vector<int> weight_vector_medium;
    std::vector<int> weight_vector_hard;

    std::string settings_file_path = std::string(SETTINGSFILE);
    std::string preset_music_dir;
};

#endif  // PROJECT_INCLUDE_SETTINGS_H