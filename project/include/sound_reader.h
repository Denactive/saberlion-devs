#ifndef SOUND_READER_H
#define SOUND_READER_H

#define SOUND_READER_LOG 0
#define DEBUG_FILE_DATA_CHECK 0

#include <iostream>
#include <string>
#include <fstream>
#include <cstdio>

typedef struct {
    size_t samples_count;
    double* samples;
} chunk;

typedef struct {
    char ID[4]; //"data" = 0x61746164
    unsigned int size;  //Chunk data bytes
} header_chunk;

typedef //Wav Header
struct {
    char chunkID[4]; //"RIFF" = 0x46464952
    unsigned int chunkSize; //28 [+ sizeof(wExtraFormatBytes) + wExtraFormatBytes] + sum(sizeof(chunk.id) + sizeof(chunk.size) + chunk.size)
    char format[4]; //"WAVE" = 0x45564157
    char subchunk1ID[4]; //"fmt " = 0x20746D66
    unsigned int subchunk1Size; //16 [+ sizeof(wExtraFormatBytes) + wExtraFormatBytes]
    unsigned short audioFormat;  // 1 = PCM, 6=mulaw ...
    unsigned short numChannels;
    unsigned int sampleRate;  // sampling freq (Hz)
    unsigned int byteRate;  // bytes per second
    unsigned short blockAlign;  // 2=16 bit mono, 4=16 bit stereo
    unsigned short bitsPerSample;
    //[WORD wExtraFormatBytes;]
    //[Extra format bytes]
} wav_header;

class Music {
public:
    Music(std::string filename);
    ~Music();

    //bool get_chunk(chunk& data);
    bool get_chunk(chunk& ch, size_t size);

    size_t get_duration() {
        return m_track_length;
    }

    size_t get_sample_rate() {
        return m_sample_rate;
    }

    size_t get_samples_count() {
        return m_samples_count;
    }

    size_t get_sound_bytes_count() {
        return m_samples_count * m_sample_size;
    }

private:
    FILE* m_file = NULL;
    size_t m_cur_pos = 0;       // указатель на курсор в файле
    size_t m_samples_count = 0; // число сэмплов в треке
    size_t m_track_length = 0;  // длина трека (в секундах)
    bool m_stereo = 0;          // флаг стерео/моно звук
    unsigned short m_num_channels = 0;     // число каналов. 2 - стерео
    unsigned int m_sample_rate = 0;        // число семплов в секунде трека (44100)
    unsigned short m_bits_per_sample = 0;  // размер одного сэмпла (в битах)
    unsigned short m_sample_size = 0;      // размер одного сэмпла (в байтах)

    void get_wav_hdr();
};

void print_wav_header(wav_header* m_header);
double u_char_to_double(const unsigned char& a, const unsigned char& b);
double* bit_to_amplitudes(const unsigned char* input, size_t size);
#endif  // SOUND_READER_H
