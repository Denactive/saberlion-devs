#ifndef PROJECT_INCLUDE_LEVEL_GENERATION_H
#define PROJECT_INCLUDE_LEVEL_GENERATION_H

#include <cmath>
#include <fftw3.h>
#include <iostream>
#include <vector>
#include <string.h>

#include "sound_reader.h"
#include "settings.h"

#define pi 3.141592

#define GENERATION_LOG 0
#define SECTION_LENGTH 10

typedef struct {
    bool hit[4];
    unsigned int time;
} obj;

fftw_complex* fftw(double *input, int N);
double* ifftw(fftw_complex *input, int N);
double** filterbank(fftw_complex *input, int N);
double** hwindow (double** sig, int N, int nbands);
double** diffrect (double** sig, int N);
int timecomb(double** sig, int N);

obj* generate_level(unsigned int& count, unsigned int& score, Settings& sets);
std::vector<unsigned int> generate_time_codes(Music& music, Settings& sets);
std::string get_lvl_filename(Settings& sets);
int save_score(Settings& sets, unsigned int score);

#endif  // #ifndef PROJECT_LEVEL_GENERATION_H