//
// updated by Sveta 20.12.2020
//

#ifndef SABERLION_DEVS_GRAPHICS_H
#define SABERLION_DEVS_GRAPHICS_H
#define GLOG 0
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <vector>
#include <cmath>

class Note {
public:
    Note(float x, float y, float width, float height, std::string file, float speed);
    void set_time_click(int new_time_click);
    int get_time_click() const;
    void set_track_number(int new_track_number);
    int get_track_number() const;
    void set_speed(float speed);
    void set_sprite(int left, int top, int width, int height);
    void set_position(float x, float y);
    void move(float x, float y);
    void set_scale(float new_scale);
    sf::Sprite get_sprite() const;
    float get_y() const;
    void set_y (float new_y);
    float get_x() const;
    float get_speed() const;
private:
    float x, y, width, height, speed;
    int time_click, track_number;
    std::string file;
    sf::Image image;
    sf::Texture texture;
    sf::Sprite sprite;
};

#endif // SABERLION_DEVS_GRAPHICS_H
