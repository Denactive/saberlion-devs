#ifndef PROJECT_INCLUDE_INTERFACE_H
#define PROJECT_INCLUDE_INTERFACE_H

#include <string.h>
#include <vector>
#include <string>

#include <stdio.h>
#include <unistd.h>
#include <dirent.h>

#include "settings.h"

#if defined(NEW_INTERFACE)
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#endif

#define SCREEN_WIDTH 650
#define SCREEN_HEIGHT 400

#define INTERFACE_LOG 1

// About Desktop OpenGL function loaders:
//  Modern desktop OpenGL doesn't have a standard portable header file to load OpenGL function pointers.
//  Helper libraries are often used for this purpose! Here we are supporting a few common ones (gl3w, glew, glad).
//  You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W) && defined(NEW_INTERFACE)
#include <GL/gl3w.h>            // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW) && defined(NEW_INTERFACE)
#include <GL/glew.h>            // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD) && defined(NEW_INTERFACE)
#include <glad/glad.h>          // Initialize with gladLoadGL()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD2) && defined(NEW_INTERFACE)
#include <glad/gl.h>            // Initialize with gladLoadGL(...) or gladLoaderLoadGL()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING2) && defined(NEW_INTERFACE)
#define GLFW_INCLUDE_NONE       // GLFW including OpenGL headers causes ambiguity or multiple definition errors.
#include <glbinding/Binding.h>  // Initialize with glbinding::Binding::initialize()
#include <glbinding/gl/gl.h>
using namespace gl;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING3) && defined(NEW_INTERFACE)
#define GLFW_INCLUDE_NONE       // GLFW including OpenGL headers causes ambiguity or multiple definition errors.
#include <glbinding/glbinding.h>// Initialize with glbinding::initialize()
#include <glbinding/gl/gl.h>
using namespace gl;
#elif defined(NEW_INTERFACE)
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif

// Include glfw3.h after our OpenGL definitions
#if defined(NEW_INTERFACE)
#include <GLFW/glfw3.h>
#endif

// [Win32] Our example includes a copy of glfw3.lib pre-compiled with VS2010 to maximize ease of testing and compatibility with old VS compilers.
// To link with VS2010-era libraries, VS2015+ requires linking with legacy_stdio_definitions.lib, which we do using this pragma.
// Your own project should not be affected, as you are likely to link with a newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS) && defined(NEW_INTERFACE)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

int render_interface(Settings& sets, bool& show_interface_window);

#if defined(NEW_INTERFACE)
static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}
static void HelpMarker(const char* desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}
#endif

// do not forget to clear a memory
static char** upload_dir_files(std::string dir, size_t& size, std::string& cur_file, size_t& cur_file_list_pos) {
    int fd = 0;
    cur_file_list_pos = -1;
    std::vector<std::string> files;

    if (INTERFACE_LOG)
        printf("upload_dir_files\n\tCurrent file: %s\n", cur_file.c_str());
    if (INTERFACE_LOG)
        printf("\tGetting files from %s\n", dir.c_str());

    if (dir != "[NULL]") {
        DIR *dir_u = opendir(dir.c_str());
        if (dir_u == NULL) {
            if (INTERFACE_LOG)
                printf("\tCan\'t open directory %s\n", dir.c_str());
            return NULL;
        } else {
            struct dirent *f_cur;
            while ((f_cur = readdir(dir_u)) != NULL) {
                std::string str(f_cur->d_name);

                // filtering .. , . , not .wav 's
                // && str.find_last_of(".wav") != std::string::npos
                std::string track_format;
                size_t dot_ptr = str.find_last_of('.');
                size_t slash_ptr = str.find_last_of('/');
                for (size_t i = dot_ptr + 1; i < str.size(); i++)
                    track_format += str[i];

                if (str != ".." && str != "." && track_format == "wav")
                    files.push_back(str);
                if (str == cur_file)
                    cur_file_list_pos = files.size() - 1;
            }
            close(fd);
            closedir(dir_u);
        }
        char** res = new char* [files.size()];
        for (size_t i = 0; i < files.size(); i++) {
            res[i] = new char [files[i].size() + 1];
            files[i].copy(res[i], files[i].size());
            res[i][files[i].size()] = '\0';
            if (INTERFACE_LOG)
                printf("\t%s\n", res[i]);
        }
        size = files.size();
        return res;
    }
    return NULL;
};

// Watch out Unicode
static char* shorten_dir(std::string s) {
    if (INTERFACE_LOG)
        printf("shorten dir\n");
    char* res = NULL;

    if (s == "[NULL]") {
        res = new char [1];
        res[0] = '\0';
        return res;
    }

    if (s.size() >= 50) {
        res = new char[50];
        for (int i = 3; i < 50; i++)
            res[i] = s [s.size() - 47 + i];
        for (int i = 0; i < 3; i++)
            res[i] = '.';
    } else {
        res = new char [s.size()];
        s.copy(res, s.size());
    }
    if (INTERFACE_LOG)
        printf("\tuser_dir_short is %s\n", res);
    return res;
}

#endif  // #ifndef PROJECT_INCLUDE_INTERFACE_H