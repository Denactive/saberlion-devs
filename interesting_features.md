## Ветки ##
История того, как развивался проект


**in-module** - вся файловая система без графики, диалоговое окно выбора файлов с сортировкой из 2х директорий (Gui)

**module graphics** - сырая графика. Добавить фильтрацию ввода (кликов)

**file_system** - попытка интеграции in-module с графикой. Исправлена фильтрация ввода в
	меню (довольно тупо через счетчик), баг с сфмл-окном при вызове interface_render( )

**beat-detection** - анализ темпа музыки

**Testing-light** - обкатывание облегченного алгоритма

**sound_spectrum_analyzer** - чтение .wav - файлов, применение Фурье

### Дополнительно ###

**music-for-test** - подборка вав-треков

**presentations** - презентации SaberLionDevs

**initial** - линтеры для старта работы

## Разработчики ##

https://trello.com/b/AZZeztu2/saberlion-developers

Света Очереретная vk.com/svetlanlka

Денис Турчин vk.com/denactive

Юра Любский vk.com/ylyubskiy
